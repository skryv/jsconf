'use strict';
angular.module('musicApp.library')
  .service('libraryModel', function (artistRepository) {
    var library = [];

    /**
     * This is the exposed music library
     */
    this.library = library;

    /**
     * Retrieve the music library
     */
    this.getLibrary = function () {
      // Retrieve the music library from our backend
      artistRepository.getAllArtists().then(function (response) {
        var artists = response.data;
        // we get the info per artist in json format
        // what we want is a flat json structure of all the songs available.
        // so we loop over each artists, the albums the artists published and the songs in each album
        angular.forEach(artists, function (artist) {
          // loop over each artist
          angular.forEach(artist.albums, function (album) {
            // loop over each album of the artist
            angular.forEach(album.songs, function (song) {
              // add each song to our library
              this.push({ artist: artist.name, album: album.title, title: song.title, length: song.length });
            }, library);
          });
        });
      });
    };

    // Retrieve all songs
    this.getLibrary();
  }
);
