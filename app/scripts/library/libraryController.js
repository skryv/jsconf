'use strict';
angular.module('musicApp.library')
  .controller('LibraryCtrl', function ($scope, libraryModel) {
    $scope.libraryModel = libraryModel;
  }
);
