'use strict';
angular.module('musicApp.library')
  .factory('artistRepository', function ($http) {
    var artistRepository = {};

    artistRepository.getAllArtists = function () {
      return $http.get('songs/music.json');
    };

    return artistRepository;
  }
);
