'use strict';
angular.module('musicApp.playlists')
  .controller('PlaylistsCtrl', function ($scope, playlistsModel) {
    $scope.playlistsModel = playlistsModel;

    $scope.activatePlaylist = function (playlist) {
      playlistsModel.setSelectedPlaylist(playlist);
    };

    $scope.removePlaylist = function (playlist) {
      playlistsModel.removePlaylist(playlist);
    };

    $scope.createPlaylist = function (playlistName) {
      playlistsModel.addPlaylist({ 'name': playlistName, 'songs': [] });
      $scope.newplaylist = '';
    };

    $scope.addSongToPlaylist = function (playlist, song) {
      playlistsModel.addSongToPlaylist(playlist, song);
    };

    $scope.inPlaylist = function (playlist, song) {
      playlistsModel.inPlaylist(playlist, song);
    };

    $scope.removeSongFromPlaylist = function (song) {
      playlistsModel.removeSongFromPlaylist(playlistsModel.selectedPlaylist, song);
    };
  }
);
