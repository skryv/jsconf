'use strict';
angular.module('musicApp.playlists')
  .factory('playlistsRepository', function ($q) {
    var playlistsRepository = {};

    var dummyData = [
      {
        'name': 'PlayListOne',
        'songs': [
          { 'artist': 'Daft Punk', 'album': 'Discovery', 'title': 'One More Time', 'length': '5:20' },
          { 'artist': 'The Beatles', 'album': 'Let It Be', 'title': 'Let It Be', 'length': '3:42' },
          { 'artist': 'Iron Maiden', 'album': 'Powerslave', 'title': '2 Minutes to Midnight', 'length': '6:04' }
        ]
      },
      {
        'name': 'Classics',
        'songs': [
          { 'artist': 'The Beatles', 'album': 'Yellow Submarine', 'title': 'Yellow Submarine', 'length': '2:40' },
          { 'artist': 'The Beatles', 'album': 'Let It Be', 'title': 'Let It Be', 'length': '3:42' },
          { 'artist': 'Iron Maiden', 'album': 'The Number of the Beast', 'title': 'The Number of the Beast', 'length': '4:25' },
          { 'artist': 'Iron Maiden', 'album': 'The Number of the Beast', 'title': 'Hallowed Be Thy Name', 'length': '7:08' }
        ]
      }
    ];

    // Retrieve my music from local storage
    playlistsRepository.getMyPlaylists = function () {
      return $q.when(dummyData);
    };

    // add song to local storage
    playlistsRepository.addSong = function () {
      return $q.reject('Not implemented yet');
    };

    // remove song from local storage
    playlistsRepository.removeSong = function () {
      return $q.reject('Not implemented yet');
    };

    return playlistsRepository;
  }
);
