'use strict';
angular.module('musicApp.playlists')
  .service('playlistsModel', function (playlistsRepository) {
    var playlists = [];

    this.playlists = playlists;
    this.selectedPlaylist = null;
    this.setSelectedPlaylist = function (playlist) {
      if (this.playlists.indexOf(playlist) > -1) {
        this.selectedPlaylist = playlist;
      }
    };

    this.getMyPlaylists = function () {
      playlistsRepository.getMyPlaylists().then(updateMyPlaylists);
    };

    this.removePlaylist = function (playlist) {
      // remove the playlist
      if (playlists.indexOf(playlist) > -1) {
        playlists.splice(playlists.indexOf(playlist), 1);
      }
    };

    this.addPlaylist = function (playlist) {
      playlists.push(playlist);
    };

    this.removeSongFromPlaylist = function (playlist, song) {
      // remove the song from the selected playlist
      if (playlist.songs.indexOf(song) > -1) {
        playlist.songs.splice(playlist.songs.indexOf(song), 1);
      }
    };

    this.addSongToPlaylist = function (playlist, song) {
      if (playlists.indexOf(playlist) > -1) {
        playlists[ playlists.indexOf(playlist) ].songs.push(song);
      }
    };

    this.inPlaylist = function (playlist, song) {
      var found = false;
      angular.forEach(playlists, function (_playlist) {
        if (_playlist === playlist) {
          angular.forEach(_playlist.songs, function (_song) {
            if (_song.title === song.title && _song.artist === song.artist && _song.album === song.album) {
              found = true;
            }
          });
        }
      });
      return found;
    };

    function updateMyPlaylists(data) {
      playlists.length = 0;

      angular.forEach(data, function (song) {
        // add each song to my music
        this.push(song);
      }, playlists);
    }

    // Retrieve my songs
    this.getMyPlaylists();
  }
);
