'use strict';
angular.module('musicApp')
  .controller('MainMenuCtrl', function ($scope, $location) {
    $scope.currentNav = $location.path();

    $scope.$on('$routeChangeSuccess', function () {
      $scope.currentNav = $location.path();
    });
  }
);
