'use strict';
angular.module('musicApp')
  .controller('IntroCtrl', function ($scope) {
    $scope.message = 'Hello, ';
    $scope.name = 'Dimitry';

    $scope.resetName = function () {
      $scope.name = 'John';
    };
  }
);
