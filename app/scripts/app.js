'use strict';

/**
 * @ngdoc overview
 * @name jsconfApp
 * @description
 * # jsconfApp
 *
 * Main module of the application.
 */
angular
  .module('musicApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'musicApp.library',
    'musicApp.mymusic',
    'musicApp.playlists',
    'LocalStorageModule'
  ])
  .config([ 'localStorageServiceProvider', function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('musicApp');
  } ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/intro', {
        templateUrl: 'views/intro.html',
        controller: 'IntroCtrl'
      })
      .when('/library', {
        templateUrl: 'views/library.html',
        controller: 'LibraryCtrl'
      })
      .when('/myMusic', {
        templateUrl: 'views/myMusic.html',
        controller: 'MyMusicCtrl'
      })
      .when('/playlists', {
        templateUrl: 'views/playlists.html',
        controller: 'PlaylistsCtrl'
      })
      .otherwise({
        redirectTo: '/library'
      });
  });
