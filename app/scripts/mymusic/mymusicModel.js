'use strict';
angular.module('musicApp.mymusic')
  .service('myMusicModel', function (mymusicRepository) {
    var myMusic = [];

    this.myMusic = myMusic;

    this.addSong = function (song) {
      // Add song to our personal library
      myMusic.push(song);
      // synchronize our library with the server
      mymusicRepository.saveMyMusic(myMusic);
    };

    this.removeSong = function (song) {
      // remove the song from our personal library
      myMusic.splice(myMusic.indexOf(song), 1);
      // synchronize our library with the server
      mymusicRepository.saveMyMusic(myMusic);
    };

    this.getMyMusic = function () {
      mymusicRepository.getMyMusic().then(updateMyMusic);
    };

    this.inCollection = function (song) {
      // This matches better than .indexOf
      var found = false;
      angular.forEach(myMusic, function (myCollection) {
        if (myCollection.title === song.title && myCollection.artist === song.artist && myCollection.album === song.album) {
          found = true;
        }
      });
      return found;
    };

    function updateMyMusic(data) {
      myMusic.length = 0;

      angular.forEach(data, function (song) {
        // add each song to my music
        this.push(song);
      }, myMusic);
    }

    // Retrieve my songs
    this.getMyMusic();
  }
);
