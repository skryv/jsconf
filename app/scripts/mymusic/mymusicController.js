'use strict';
angular.module('musicApp.mymusic')
  .controller('MyMusicCtrl', function ($scope, myMusicModel) {
    $scope.myMusicModel = myMusicModel;

    $scope.addSong = function (song) {
      myMusicModel.addSong(song);
    };

    $scope.removeSong = function (song) {
      myMusicModel.removeSong(song);
    };

    $scope.inCollection = function (song) {
      return myMusicModel.inCollection(song);
    };
  }
);
