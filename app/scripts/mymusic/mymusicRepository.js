'use strict';
angular.module('musicApp.mymusic')
  .factory('mymusicRepository', function ($q, localStorageService) {
    var mymusicRepository = {};

    mymusicRepository.getMyMusic = function () {
      return $q.when(localStorageService.get('mymusic'));
    };

    mymusicRepository.saveMyMusic = function (myMusic) {
      localStorageService.set('mymusic', myMusic);
    };

    return mymusicRepository;
  }
);
