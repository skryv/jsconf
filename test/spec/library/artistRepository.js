'use strict';

describe('Library: artistRepository', function () {

  var artistRepository, $http;

  beforeEach(module('musicApp.library'));

  beforeEach(inject(function (_artistRepository_, _$http_) {
    artistRepository = _artistRepository_;
    $http = _$http_;
  }));

  it('should get an instance of artistRepository', function() {
    expect(artistRepository).toBeDefined();
  });

  it('should return a http request', function() {
    //expect(artistRepository.getAllArtists()).toEqual($http.get('songs/music.json'));
  });
});
