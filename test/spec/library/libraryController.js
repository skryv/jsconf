'use strict';

describe('Library: libraryController', function () {

  var $controller, $scope, libraryModel;

  beforeEach(module('musicApp.library'));

  beforeEach(inject(function (_$controller_, $rootScope, _libraryModel_) {
    $scope = $rootScope.$new();
    libraryModel = _libraryModel_;
    $controller = _$controller_('LibraryCtrl', {
      $scope: $scope,
      libraryModel: libraryModel
    });
  }));

  it('should return library model to scope', function () {
    expect($scope.libraryModel).toEqual(libraryModel);
  });
});
