describe('JSConf Music App Intro Page', function() {
  it('should show the intro page', function() {
    browser.get('http://localhost:9000/#/intro');

    var reset = element(by.id('reset'));
    reset.click();

    var name = element(by.css('h2'));
    expect(name.getText()).toEqual('Hello, John');
  });
});
