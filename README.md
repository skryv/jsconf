# JSConf 2015 - AngularJS #
This repository is part of a workshop for JSConf 2015.  
A google slides is available that complements this repo here:  https://docs.google.com/presentation/d/1dJh10Tw1dHZXYvc_btqbGFPwu3AKsdbmB3SMpk6j7Qo/edit#slide=id.g997e0d0f0_3_8

The master branch contains the full example, for each step there is a branch available that corresponds with the google slides.  
If at any time you get stuck you can reset to the step your at and retry by issuing: 'git reset --hard origin/step-#'

# Setting everything up #
### Prerequisites ###
Make sure the following tools are installed before continuing.  
node --version Should be v0.10.x+  
npm --version Should be v2.1.0+  
git --version Should be 1.9.0+  
yo --version Should be 1.4.6  
bower --version Should be 1.4.1  
grunt --version Should be grunt-cli v0.1.13  

Install what's missing:  
Ruby for your OS  
Update NPM: npm cache clean -f && npm install -g n && n stable  
Install tools: npm install --global yo bower grunt-cli  

### Install dependencies ####
Check out this repo, cd to the directory and run the following commands  
npm install  
bower install  

### Test it ###
grunt serve

### Who do I talk to? ###
* dimitry.declercq@skryv.com  
* gertjan.vermeir@skryv.com  
* toon.timbermont@skryv.com